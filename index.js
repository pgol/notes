const express = require('express');
const bodyParser = require('body-parser');
const models = require('./models');
const Note = models.note;
const app = express();
const router = express.Router();
const port = process.env.PORT || 3002;
const cors = require('cors');

models.sequelize.sync()
  .then(() => {
    app.listen(port, () => console.log(`Server running on port ${port}`))
  });


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.use((req, res, next) => {
  console.log(req);
  next();
});
app.use('/api/v1', router);

router.get('/notes', (req, res) => {
  Note.findAll()
    .then(notes => res.json(notes));
});

router.get('/notes/:id', (req, res) => {
  Note.find({
    where: {
      id: req.params.id
    }
  })
    .then(note => {
      if (note) {
        res.json(note);
      } else {
        res.send(404)
      }
    });
});
// /api/v1/notes
router.post('/notes', (req, res) => {
  Note.create({
    title: req.body.title,
    text: req.body.text,
  })
    .then(note => res.json(note));
});

router.put('/notes/:id', (req, res) => {
  Note.find({
    where: {
      id: req.params.id
    }
  })
    .then(note => {
      if (note) {
        return note.updateAttributes({
          title: req.body.title,
          text: req.body.text
        })
      }
    })
    .then(updatedNote => res.json(updatedNote));
});


router.delete('/notes/:id', (req, res) => {
  Note.destroy({
    where: {
      id: req.params.id
    }
  })
    .then(removedNote => res.json(removedNote));
});




